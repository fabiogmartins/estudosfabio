﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.GlobalDBM.Depositos.Models;


namespace Mega.Erp.GlobalDBM.Depositos.Interfaces
{
    public interface IDepositoRepository
    {
        IEnumerable<Deposito> List();
        Deposito Get(int id);
        IEnumerable<Filial> GetFiliais(int id);
        IEnumerable<CodigoEstrutura> GetBlocos(int id);
        Deposito Insert(Deposito deposito);
        CodigoEstrutura InsertDepositoBloco(Deposito deposito, CodigoEstrutura bloco);
        Filial InsertDepositoFilial(Deposito deposito, Filial filial);
        Deposito Update(Deposito deposito);
        void Delete(int id);
        void DeleteBloco(Deposito deposito, CodigoEstrutura bloco);
        void DeleteFilial(Deposito deposito, Filial filial);
    }
}
