﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;

namespace Mega.Erp.GlobalDBM.Depositos.Models
{
    public enum Tipo
    {
        Global = 'G',
        Bloco = 'B', //Se for Bloco, tem vinculado uma lista de Estruturas
        Filial = 'F' //Se for Filial, tem vinculado uma lista de Filiais
    }
    public class Acesso
    {
        public Tipo Tipo { get; set; }
        public IEnumerable<CodigoEstrutura> BlocosVinculados { get; set; }
        public IEnumerable<Filial> FiliaisVinculadas { get; set; }
    }
}