﻿namespace Mega.Erp.GlobalDBM.Depositos.Models
{
    public class CodigoEstrutura
    {
        public int Id { get; set; }
        public Codigo Codigo { get; set; }
    }
}