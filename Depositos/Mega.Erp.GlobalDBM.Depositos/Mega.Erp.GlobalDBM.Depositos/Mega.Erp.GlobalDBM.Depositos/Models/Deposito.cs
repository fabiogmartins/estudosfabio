﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.Depositos.Models
{
     public enum TipoStatus
     {
        Disponivel = 'D',
        Indisponivel = 'I'
     }
    public class Deposito
    {
        public int Id { get; set; } 
        public string Nome { get; set; }
        public DateTime? DataCadastro { get; set; }
        public decimal Area { get; set; }
        public TipoStatus Status { get; set; }
        public CodigoEstrutura CodigoEstrutura { get; set; }
        public Acesso Acesso { get; set; }
    }
}
