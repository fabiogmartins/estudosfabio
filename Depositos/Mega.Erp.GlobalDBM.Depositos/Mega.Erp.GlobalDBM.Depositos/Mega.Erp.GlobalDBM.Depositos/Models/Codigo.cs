﻿namespace Mega.Erp.GlobalDBM.Depositos.Models
{
    public class Codigo
    {
        public int Id { get; set; }
        public int Tabela { get; set; }
        public int Padrao { get; set; }
        public string Tipo { get; set; }
    }
}