﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mega.Erp.GlobalDBM.Depositos.Models;
using Mega.Erp.GlobalDBM.Depositos.Service;

namespace Mega.Erp.GlobalDBM.Depositos.Controllers
{
    [Route("api/[controller]")]
    public class DepositoController : Controller
    {

        private DepositoService _service;

        public DepositoController(DepositoService service)
        {
            _service = service;
        }


        // GET api/values
        [HttpGet]
        public IEnumerable<Deposito> Get()
        {
            return _service.List();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Deposito Get(int id)
        {
            return _service.Get(id);
        }

        // POST api/values
        [HttpPost]
        public Deposito Post([FromBody]Deposito value)
        {
            return _service.Insert(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Deposito value)
        {
            _service.Update(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _service.Delete(id);
        }
    }
}
