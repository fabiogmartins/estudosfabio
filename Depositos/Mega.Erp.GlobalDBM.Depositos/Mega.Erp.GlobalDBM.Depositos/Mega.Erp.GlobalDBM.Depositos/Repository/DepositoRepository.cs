﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.GlobalDBM.Depositos.Interfaces;
using System.Data.Common;
using Dapper;
using Mega.Erp.GlobalDBM.Depositos.Models;

namespace Mega.Erp.GlobalDBM.Depositos.Repository
{
    public class DepositoRepository : IDepositoRepository
    {
        //Declaração das consultas SQL
        private string _selectDeposito => @"select d.dep_in_codigo Id,
                                                   d.dep_st_nome Nome,
                                                   d.dep_dt_cadastro DataCadastro,
                                                   d.dep_re_area Area,
                                                   ascii(d.dep_ch_status) Status,
                                                   d.est_in_codigo Id,                                                   
                                                   d.org_in_codigo Id,                                                   
                                                   d.org_tab_in_codigo Tabela,
                                                   d.org_pad_in_codigo Padrao,
                                                   d.org_tau_st_codigo Tipo,
                                                   ascii(d.dep_ch_tipoacesso) Tipo 
                                              from mgdbm.dbm_deposito d";

        private string _selectBlocos => @"select dp.est_in_codigo Id,
                                                 dp.org_in_codigo Id,
                                                 dp.org_tab_in_codigo Tabela,
                                                 dp.org_pad_in_codigo Padrao,
                                                 dp.org_tau_st_codigo Tipo
                                            from mgdbm.dbm_deposito_bloco dp";
        private string _selectFiliais => @"select df.gru_in_codigo Usuario,
                                                  df.org_in_codigo Id,
                                                  df.org_tab_in_codigo Tabela,
                                                  df.org_pad_in_codigo Padrao,
                                                  df.org_tau_st_codigo Tipo
                                            from mgdbm.dbm_deposito_filial df";
        //Fim das declarações de consultas SQL

        private DbConnection _connect;
        public DepositoRepository(DbConnection connect)
        {
            _connect = connect;
        }

        public Deposito PopulaDeposito(dynamic valoresDeposito)
        {
            var deposito = new Deposito();
            deposito = Get(valoresDeposito.ID);
            deposito.Acesso.BlocosVinculados = GetBlocos(valoresDeposito.ID);
            deposito.Acesso.FiliaisVinculadas = GetFiliais(valoresDeposito.ID);

            return deposito;
        }

        public IEnumerable<Deposito> List()
        {
            foreach (dynamic valoresDeposito in _connect.Query<dynamic>($@"{_selectDeposito}"))
            {
                yield return PopulaDeposito(valoresDeposito);
            }
        }


        public Deposito Get(int id)
        {
            return _connect.Query<Deposito, CodigoEstrutura, Codigo, Acesso, Deposito>($@"{_selectDeposito}
                                                                                       where dep_in_codigo = :id",
                                                                                       (deposito, codigoestrutura, codigo, acesso) =>
                                                                                       {
                                                                                           deposito.CodigoEstrutura = codigoestrutura;
                                                                                           codigoestrutura.Codigo = codigo;
                                                                                           deposito.Acesso = acesso;
                                                                                           return deposito;
                                                                                       },
                                                                                       new { id }
                                                                                       ,splitOn: "Id,Id,Id,Tipo").Single();
        }

        public IEnumerable<Filial> GetFiliais(int id)
        {
            return _connect.Query<Filial,Codigo,Filial>($@"{_selectFiliais}
                                                       where df.dep_in_codigo = :id", 
                                                       (filial, codigo) =>
                                                       {
                                                           filial.Codigo = codigo;
                                                           return filial;
                                                       },
                                                       new { id }
                                                       , splitOn: "Usuario,Id");
        }

        public IEnumerable<CodigoEstrutura> GetBlocos(int id)
        {
            return _connect.Query<CodigoEstrutura,Codigo,CodigoEstrutura>($@"{_selectBlocos}
                                                                          where dp.dep_in_codigo = :id",
                                                                          (codigoestrutura, codigo) =>
                                                                          {
                                                                              codigoestrutura.Codigo = codigo;
                                                                              return codigoestrutura;
                                                                          },
                                                                          new { id }
                                                                          , splitOn: "Id,Id");
        }



        public Deposito Insert(Deposito deposito)
        {
            _connect.Execute(@"insert into mgdbm.dbm_deposito(dep_in_codigo, dep_st_nome, dep_dt_cadastro, dep_re_area, dep_ch_status, 
                                                              org_tab_in_codigo, org_pad_in_codigo, org_in_codigo, org_tau_st_codigo, est_in_codigo,
                                                              dep_ch_tipoacesso)
                                                        values
                                                             (:Id, :Nome, :DataCadastro, :Area, chr(:Status), 
                                                              :EmpTabela, :EmpPadrao, :EmpCodigo, :EmpTipo, :EmpId, 
                                                              chr(:Acesso))", new
                                                        {
                                                            Id = deposito.Id,
                                                            Nome = deposito.Nome,
                                                            DataCadastro = deposito.DataCadastro,
                                                            Area = deposito.Area,
                                                            Status = deposito.Status,
                                                            EmpTabela = deposito.CodigoEstrutura.Codigo.Tabela,
                                                            EmpPadrao = deposito.CodigoEstrutura.Codigo.Padrao,
                                                            EmpCodigo = deposito.CodigoEstrutura.Codigo.Id,
                                                            EmpTipo = deposito.CodigoEstrutura.Codigo.Tipo,
                                                            EmpId = deposito.CodigoEstrutura.Id,
                                                            Acesso = deposito.Acesso.Tipo
                                                        });
            
            return deposito;
        }

        public CodigoEstrutura InsertDepositoBloco(Deposito deposito, CodigoEstrutura bloco)
        {
            _connect.Execute(
                    @"insert into mgdbm.dbm_deposito_bloco
                        (org_tab_in_codigo,
                         org_pad_in_codigo,
                         org_in_codigo,
                         org_tau_st_codigo,
                         est_in_codigo,
                         dep_in_codigo)
                    values
                        (:BloTabela,
                         :BloPadrao,
                         :BloCodigo,
                         :BloTipo,
                         :BloId,
                         :DepId)
                    ", new
                    {
                        BloTabela = bloco.Codigo.Tabela,
                        BloPadrao = bloco.Codigo.Padrao,
                        BloCodigo = bloco.Codigo.Id,
                        BloTipo = bloco.Codigo.Tipo,
                        BloId = bloco.Id,
                        DepId = deposito.Id
                    });

            return bloco;
        }

        public Filial InsertDepositoFilial(Deposito deposito, Filial filial)
        {
            _connect.Execute(
                    @"insert into mgdbm.dbm_deposito_filial
                        (org_tab_in_codigo,
                         org_pad_in_codigo,
                         org_in_codigo,
                         org_tau_st_codigo,
                         gru_in_codigo,
                         dep_in_codigo)
                    values
                        (:BloTabela,
                         :BloPadrao,
                         :BloCodigo,
                         :BloTipo,
                         :Usuario,
                         :DepId)
                    ", new
                    {
                        BloTabela = filial.Codigo.Tabela,
                        BloPadrao = filial.Codigo.Padrao,
                        BloCodigo = filial.Codigo.Id,
                        BloTipo  = filial.Codigo.Tipo,
                        Usuario  = filial.Usuario,
                        DepId     = deposito.Id
                    });
            return filial;
        }

        public Deposito Update(Deposito deposito)
        {
            _connect.Execute(
                @"update mgdbm.dbm_deposito
                     set dep_st_nome = :Nome,
                         dep_dt_cadastro = :DataCadastro,
                         dep_re_area = :Area,
                         dep_ch_status = chr(:Status),
                         org_tab_in_codigo = :EmpTabela,
                         org_pad_in_codigo = :EmpPadrao,
                         org_in_codigo = :EmpCodigo,
                         org_tau_st_codigo = :EmpTipo,
                         est_in_codigo = :EmpId,
                         dep_ch_tipoacesso = chr(:Acesso)
                   where dep_in_codigo = :Id"
                , new
                {
                    Id = deposito.Id,
                    Nome = deposito.Nome,
                    DataCadastro = deposito.DataCadastro,
                    Area = deposito.Area,
                    Status = deposito.Status,
                    EmpTabela = deposito.CodigoEstrutura.Codigo.Tabela,
                    EmpPadrao = deposito.CodigoEstrutura.Codigo.Padrao,
                    EmpCodigo = deposito.CodigoEstrutura.Codigo.Id,
                    EmpTipo = deposito.CodigoEstrutura.Codigo.Tipo,
                    EmpId = deposito.CodigoEstrutura.Id,
                    Acesso = deposito.Acesso.Tipo
                });
            return deposito;
        }

        public void Delete(int id)
        {
            _connect.Execute(
                @"delete mgdbm.dbm_deposito
                   where dep_in_codigo = :Id"
                , new { Id = id });
        }

        public void DeleteBloco(Deposito deposito, CodigoEstrutura bloco)
        {
            _connect.Execute(
                @"delete mgdbm.dbm_deposito_bloco
                   where org_tab_in_codigo = :EmpTabela
                     and org_pad_in_codigo = :EmpPadrao
                     and org_in_codigo     = :EmpCodigo
                     and org_tau_st_codigo = :EmpTipo
                     and est_in_codigo     = :EmpId
                     and dep_in_codigo     = :DepId"
                , new {
                        EmpTabela = bloco.Codigo.Tabela,
                        EmpPadrao = bloco.Codigo.Padrao,
                        EmpCodigo = bloco.Codigo.Id,
                        EmpTipo = bloco.Codigo.Tipo,
                        EmpId = bloco.Id,
                        DepId = deposito.Id
                    });
           
        }

        public void DeleteFilial(Deposito deposito, Filial filial)
        {
            _connect.Execute(
                @"delete mgdbm.dbm_deposito_filial
                   where org_tab_in_codigo = :BloTabela
                     and org_pad_in_codigo = :BloPadrao
                     and org_in_codigo     = :BloCodigo
                     and org_tau_st_codigo = :BloTipo
                     and gru_in_codigo     = :Usuario
                     and dep_in_codigo     = :DepId"
                , new
                {
                    BloTabela = filial.Codigo.Tabela,
                    BloPadrao = filial.Codigo.Padrao,
                    BloCodigo = filial.Codigo.Id,
                    BloTipo = filial.Codigo.Tipo,
                    Usuario = filial.Usuario,
                    DepId = deposito.Id
                });
        }
    }

}
