﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.GlobalDBM.Depositos.Models;
using Mega.Erp.GlobalDBM.Depositos.Interfaces;

namespace Mega.Erp.GlobalDBM.Depositos.Service
{
    public class DepositoService
    {
        private IDepositoRepository _repository;
        

        public DepositoService(IDepositoRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Deposito> List()
        {
            return _repository.List();
        }

        public Deposito Get(int id)
        {
            Deposito deposito = _repository.Get(id);
            deposito.Acesso.BlocosVinculados  = _repository.GetBlocos(id);
            deposito.Acesso.FiliaisVinculadas = _repository.GetFiliais(id);
            return deposito;
        }

        public Deposito Insert(Deposito deposito)
        {
            Deposito depositoIns = _repository.Insert(deposito);
            if (deposito.Acesso.Tipo == Tipo.Bloco) 
            {
                foreach (CodigoEstrutura bloco in depositoIns.Acesso.BlocosVinculados)
                {
                    _repository.InsertDepositoBloco(depositoIns, bloco);
                }

            }

            if (deposito.Acesso.Tipo == Tipo.Filial)
            {
                foreach (Filial filial in depositoIns.Acesso.FiliaisVinculadas)
                {
                    _repository.InsertDepositoFilial(depositoIns, filial);
                }

            }
            return depositoIns;
        }

        public Deposito Update(Deposito deposito)
        {
            _repository.Update(deposito);

            IEnumerable<Filial> filiaisgravadas = _repository.GetFiliais(deposito.Id);

            //Percorre a Lista de Filiais Gravadas no Banco e compara com a Lista de Filiais passadas por parametro
            //Se exitir a filial na listagem gravada e nãos existir na listagem de filiais passadas por parametro, realiza a exclusão
            foreach (Filial filialgravada in filiaisgravadas)
            {
                bool filialexcluida = true;
                foreach (Filial filialupdate in deposito.Acesso.FiliaisVinculadas)
                {
                    if (filialgravada.Codigo.Id     == filialupdate.Codigo.Id &&
                        filialgravada.Codigo.Padrao == filialupdate.Codigo.Padrao &&
                        filialgravada.Codigo.Tabela == filialupdate.Codigo.Tabela &&
                        filialgravada.Codigo.Tipo   == filialupdate.Codigo.Tipo &&
                        filialgravada.Usuario       == filialupdate.Usuario)
                    {
                        filialexcluida = false;  
                    }
                }
                if (filialexcluida == true)
                {
                    _repository.DeleteFilial(deposito, filialgravada);
                }
            }

            //Percorre a Lista de Filiais Gravadas no Banco e compara com a Lista de Filiais passadas por parametro
            //Se não exitir a filial na listagem gravada e existir na listagem de filiais passadas por parametro, realiza a inclusão
            foreach (Filial filialupdate in deposito.Acesso.FiliaisVinculadas) 
            {
                bool filialinserida = true;
                foreach (Filial filialgravada in filiaisgravadas)
                {
                    if (filialgravada.Codigo.Id == filialupdate.Codigo.Id &&
                        filialgravada.Codigo.Padrao == filialupdate.Codigo.Padrao &&
                        filialgravada.Codigo.Tabela == filialupdate.Codigo.Tabela &&
                        filialgravada.Codigo.Tipo == filialupdate.Codigo.Tipo &&
                        filialgravada.Usuario == filialupdate.Usuario)
                    {
                        filialinserida = false;
                    }
                }
                if (filialinserida == true)
                {
                    _repository.InsertDepositoFilial(deposito, filialupdate);
                }
            }


            IEnumerable<CodigoEstrutura> blocosgravadas = _repository.GetBlocos(deposito.Id);


            //Percorre a Lista de Blocos Gravados no Banco e compara com a Lista de Blocos passados por parametro
            //Se exitir o Bloco na listagem gravada e não existir na listagem de Blocos passadas por parametro, realiza a exclusão
            foreach (CodigoEstrutura blocogravado in blocosgravadas)
            {
                bool blocoexcluido = true;
                foreach (CodigoEstrutura blocoupdate in deposito.Acesso.BlocosVinculados)
                {
                    if (blocogravado.Codigo.Id == blocoupdate.Codigo.Id &&
                        blocogravado.Codigo.Padrao == blocoupdate.Codigo.Padrao &&
                        blocogravado.Codigo.Tabela == blocoupdate.Codigo.Tabela &&
                        blocogravado.Codigo.Tipo == blocoupdate.Codigo.Tipo &&
                        blocogravado.Id == blocoupdate.Id)
                    {
                        blocoexcluido = false;
                    }
                }
                if (blocoexcluido == true)
                {
                    _repository.DeleteBloco(deposito, blocogravado);
                }
            }

            //Percorre a Lista de Blocos Gravados no Banco e compara com a Lista de Blocos passados por parametro
            //Se não exitir o Bloco na listagem gravada e existir na listagem de Blocos passados por parametro, realiza a inclusão
            foreach (CodigoEstrutura blocoupdate in deposito.Acesso.BlocosVinculados)
            {
                bool filialinserida = true;
                foreach (CodigoEstrutura blocogravado in blocosgravadas)
                {
                    if (blocogravado.Codigo.Id == blocoupdate.Codigo.Id &&
                        blocogravado.Codigo.Padrao == blocoupdate.Codigo.Padrao &&
                        blocogravado.Codigo.Tabela == blocoupdate.Codigo.Tabela &&
                        blocogravado.Codigo.Tipo == blocoupdate.Codigo.Tipo &&
                        blocogravado.Id == blocoupdate.Id)
                    {
                        filialinserida = false;
                    }
                }
                if (filialinserida == true)
                {
                    _repository.InsertDepositoBloco(deposito, blocoupdate);
                }
            }


            return deposito;
        }


        public void Delete(int id)
        {
            _repository.Delete(id);
        }

    }
}
