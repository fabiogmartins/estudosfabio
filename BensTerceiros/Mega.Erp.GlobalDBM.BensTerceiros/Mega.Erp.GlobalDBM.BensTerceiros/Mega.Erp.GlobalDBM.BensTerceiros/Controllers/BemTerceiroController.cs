﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mega.Erp.GlobalDBM.BensTerceiros.Services;
using Mega.Erp.GlobalDBM.BensTerceiros.Models;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Controllers
{
    [Route("api/[controller]")]
    public class BemTerceiroController : Controller
    {
        private BemTerceiroService _service;

        public BemTerceiroController(BemTerceiroService service)
        {
            _service = service;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<BemTerceiro> Get()
        {
            return _service.List();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public BemTerceiro Get(int id)
        {
            return _service.Get(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]BemTerceiro value)
        {
            _service.Insert(value);
        }

        // PUT api/values/5
        //[HttpPut("{id}")]
        [HttpPut]
        public void Put(int id, [FromBody]BemTerceiro value)
        {
            _service.Update(value);
        }

        // DELETE api/values/5
        //[HttpDelete("{id}")]
        [HttpDelete]
        public void Delete(BemTerceiro value)
        {
            _service.Delete(value);
        }
    }
}
