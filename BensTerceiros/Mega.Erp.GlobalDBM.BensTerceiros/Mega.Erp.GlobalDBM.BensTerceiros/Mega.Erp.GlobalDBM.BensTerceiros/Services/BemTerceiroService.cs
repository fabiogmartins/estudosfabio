﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.GlobalDBM.BensTerceiros.Interfaces;
using Mega.Erp.GlobalDBM.BensTerceiros.Models;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Services
{
    public class BemTerceiroService 
    {
        private IBemTerceiroRepository _repository;
        public BemTerceiroService(IBemTerceiroRepository repository)
        {
            _repository = repository;
        }
        public IEnumerable<BemTerceiro> List()
        {
            IEnumerable<BemTerceiro> bensterceiro = _repository.List();

            for (int i = 0; i < bensterceiro.Count(); i++)
            {
                bensterceiro.ElementAt(i).Avaliacoes = _repository.GetAvaliacoes(bensterceiro.ElementAt(i).Id);
                bensterceiro.ElementAt(i).Despesas   = _repository.GetDespesas(bensterceiro.ElementAt(i).Id);
                bensterceiro.ElementAt(i).Contabilidade = _repository.GetContabilidade(bensterceiro.ElementAt(i).Id);
            }

            return bensterceiro;
        }

        public BemTerceiro Get(int id)
        {
            BemTerceiro bemterceiro = _repository.Get(id);
            bemterceiro.Avaliacoes  = _repository.GetAvaliacoes(id);
            bemterceiro.Despesas    = _repository.GetDespesas(id);
            bemterceiro.Contabilidade = _repository.GetContabilidade(id);

            return bemterceiro;
        }

        public BemTerceiro Insert(BemTerceiro bemterceiro)
        {
            _repository.Insert(bemterceiro);
            _repository.InsertContabilidade(bemterceiro, bemterceiro.Contabilidade);

            for (int i = 0; i < bemterceiro.Avaliacoes.Count(); i++)
            {
                _repository.InsertAvaliacao(bemterceiro, bemterceiro.Avaliacoes.ElementAt(i));
            }

            for (int i = 0; i < bemterceiro.Despesas.Count(); i++)
            {
                _repository.InsertDespesa(bemterceiro, bemterceiro.Despesas.ElementAt(i));
            }

            return bemterceiro;
        }

        public BemTerceiro Update(BemTerceiro bemterceiro)
        {
            //Duvida? Como controlar as transações? Se der erro no UpdateContabilidade, o Update anterior já foi realizado.
            _repository.Update(bemterceiro);
            _repository.UpdateContabilidade(bemterceiro, bemterceiro.Contabilidade);

            //Devera comparar a listagem de Avaliações e Despesas do Objeto com o Banco para verificar se deve excluir ou inserir (aguardando uma melhor lógica, que o Ricardo comentou no projeto de Depositos)

            return bemterceiro;
        }

        public BemTerceiro Delete(BemTerceiro bemterceiro)
        {
            _repository.Delete(bemterceiro);
            return bemterceiro;
        }
    }
}
