﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data.Common;
using Dapper;
using Mega.Erp.GlobalDBM.BensTerceiros.Models;
using Mega.Erp.GlobalDBM.BensTerceiros.Interfaces;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Repository
{
    public class BemTerceiroRepository : IBemTerceiroRepository
    {

        //Declaração das consultas SQL
        private string _selectBemTerceiro => @"select bt.bem_in_codigo Id,
                                                      bt.ind_in_codigo CodigoIndice,
                                                      bt.bem_st_nome Nome,
                                                      ascii(bt.bem_ch_status) Status,
                                                      bt.bem_dt_status DataStatus,
                                                      ascii(bt.bem_ch_quitacao) Quitacao,
                                                      ascii(bt.bem_bo_alienado) Alienado,
                                                      bt.bem_re_valorvenda ValorVenda,
                                                      bt.bem_re_valorpretendido ValorPretendido,
                                                      bt.bem_re_valorautorizado ValorAutorizado,
                                                      bt.bem_bl_observacoes Observacao,
                                                      bt.bem_re_utilizacao Utilizacao,
                                                      bt.bem_dt_cadastro DataCadastro,
                                                      bt.bem_re_valorutilizado ValorUtilizado,
                                                      bt.cnd_in_codigo CodigoProposta,
                                                      cast(bt.bemimob_st_codigo as char(2)) CodigoUnidadeImobiliaria,
                                                      bt.natbem_in_codigo CodigoNaturezaEspecifica,
                                                      ascii(bt.bem_ch_baserecsped) BaseReceitaSPED,
                                                      ascii(bt.bem_ch_participasped) ParticipaSPEDPISCOFINS,
                                                      ascii(bt.bem_bo_rectermncontrat) ReceitasNaoContratuais,
                                                      ascii(bt.bem_bo_bemgeraf500) GeraF500F525,
                                                      ascii(bt.bem_bo_consctosemrec) ContratosSemRecebimento,
                                                      cast (bt.bem_ch_spedcstpis as char(2)) SituacaoTributariaPIS,
                                                      cast (bt.bem_ch_spedcstcofins as char(2)) SituacaoTributariaCOFINS,
                                                      ascii(bt.bem_bo_spedconsideradesc) DescontoPISCOFINS,
                                                      ascii(bt.bem_bo_consencargossped) Encargos,

                                                      bt.org_in_codigo Id,
                                                      bt.org_tab_in_codigo Tabela,
                                                      bt.org_pad_in_codigo Padrao,
                                                      bt.org_tau_st_codigo TipoIndetificador,
       
                                                      bt.cus_in_reduzido Id,
                                                      bt.cus_tab_in_codigo Tabela,
                                                      bt.cus_pad_in_codigo Padrao,
                                                      bt.cus_ide_st_codigo TipoIndetificador,
       
                                                      bt.pro_in_reduzido Id,
                                                      bt.pro_tab_in_codigo Tabela,
                                                      bt.pro_pad_in_codigo Padrao,
                                                      bt.pro_ide_st_codigo TipoIndetificador,
       
                                                      bt.tbe_in_codigo CodigoTipoBem,
                                                      ascii(tbe.tbe_ch_categoria) Categoria,
                                                      bt.bem_re_imo_areatotal AreaTotal,
                                                      bt.bem_re_imo_areaconstruida AreaConstruida,
                                                      bt.bem_in_imo_dependencias Dependencias,
                                                      bt.bem_st_vei_placa Placa,
                                                      bt.bem_re_vei_kilometragem Kilometragem,
                                                      ascii(bt.bem_ch_vei_combustivel) Combustivel
       
                                                 from mgdbm.dbm_bens_terceiro bt,
                                                      mgdbm.dbm_tipo_bem tbe
                                                where bt.tbe_in_codigo = tbe.tbe_in_codigo";

        private string _selectAvaliacao => @"select a.ava_in_codigo Id,
                                                    a.ava_dt_avaliacao DataAvaliacao,
                                                    a.ava_st_avaliador NomeAvaliador,
                                                    a.ava_re_valormaximo ValorMaximo,
                                                    a.ava_re_valorminimo ValorMinimo
                                               from mgdbm.dbm_avaliacao a";

        private string _selectDespesa => @"select d.des_in_codigo Id,
                                                  d.des_re_valor Valor,
                                                  d.des_dt_cadastro DataCadastro,
                                                  d.des_st_historico Historico,
                                                  ascii(d.des_bo_agrega) Agrega
                                             from mgdbm.dbm_despesa_bem d";

        private string _selectContabilidade => @"SELECT bec.bem_in_codigo Id, 

                                                        bec.clicp_pla_in_reduzido Id,
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.clicp_pla_ide_st_codigo TipoIndetificador,
       
                                                        bec.trans_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.trans_pla_ide_st_codigo TipoIndetificador,

                                                        bec.jrocto_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.jrocto_pla_ide_st_codigo TipoIndetificador,

                                                        bec.dis_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.dis_pla_ide_st_codigo TipoIndetificador,

                                                        bec.disp_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.disp_pla_ide_st_codigo TipoIndetificador,

                                                        bec.vlrori_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.vlrori_pla_ide_st_codigo TipoIndetificador,

                                                        bec.varmon_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.varmon_pla_ide_st_codigo TipoIndetificador,

                                                        bec.per_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.per_pla_ide_st_codigo TipoIndetificador,

                                                        bec.car_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.car_pla_ide_st_codigo TipoIndetificador,

                                                        bec.bem_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.bem_pla_ide_st_codigo TipoIndetificador,

                                                        bec.ven_pla_in_reduzido Id,       
                                                        bec.pla_tab_in_codigo Tabela,
                                                        bec.pla_pad_in_codigo Padrao,
                                                        bec.ven_pla_ide_st_codigo TipoIndetificador,


                                                        bec.acaoi_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaoi_pad_in_codigo Padrao,
       
                                                        bec.acaot_in_codigo Id, 
                                                        bec.acao_tab_in_codigo Tabela, 
                                                        bec.acaot_pad_in_codigo Padrao,
       
                                                        bec.acaom_in_codigo Id, 
                                                        bec.acao_tab_in_codigo Tabela, 
                                                        bec.acaom_pad_in_codigo Padrao,
       
                                                        bec.acaod_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaod_pad_in_codigo Padrao,
       
                                                        bec.acaoe_in_codigo Id, 
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaoe_pad_in_codigo Padrao,
       
                                                        bec.acaos_in_codigo Id, 
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaos_pad_in_codigo Padrao,
       
                                                        bec.acaob_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaob_pad_in_codigo Padrao,
       
                                                        bec.acaoa_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaoa_pad_in_codigo Padrao,
       
                                                        bec.acaol_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaol_pad_in_codigo Padrao,
       
                                                        bec.acaop_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaop_pad_in_codigo Padrao,
       
                                                        bec.acaok_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaok_pad_in_codigo Padrao,
              
                                                        bec.acaog_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaog_pad_in_codigo Padrao,
       
                                                        bec.acaov_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaov_pad_in_codigo Padrao,
       
                                                        bec.acaotx_in_codigo Id,
                                                        bec.acao_tab_in_codigo Tabela,
                                                        bec.acaotx_pad_in_codigo Padrao,
       
                                                        bec.acaonc_in_codigo Id,
                                                        bec.acaonc_tab_in_codigo Tabela,
                                                        bec.acaonc_pad_in_codigo Padrao,
       
                                                        bec.acaodtn_in_codigo Id,
                                                        bec.acaodtn_tab_in_codigo Tabela,
                                                        bec.acaodtn_pad_in_codigo Padrao,
       
                                                        bec.acaodt_in_codigo Id,
                                                        bec.acaodt_tab_in_codigo Tabela,
                                                        bec.acaodt_pad_in_codigo Padrao,
       
                                                        bec.acaoet_tab_in_codigo Id,
                                                        bec.acaoet_pad_in_codigo Tabela,
                                                        bec.acaoet_in_codigo Padrao
       
                                                    FROM mgdbm.dbm_bem_contabilidade bec";

        //Fim das declarações de consultas SQL


        private DbConnection _connect;
        public BemTerceiroRepository(DbConnection connect)
        {
            _connect = connect;
        }

        public IEnumerable<BemTerceiro> List()
        {
            return _connect.Query<BemTerceiro, CodigoComposto, CodigoComposto, CodigoComposto, TipoBem, BemTerceiro>($@"{_selectBemTerceiro}",
                                                                                                                     (bemterceiro, codigocomposto, codigocompostocc, codigocompostopro, tipobem) =>
                                                                                                                     {
                                                                                                                         bemterceiro.CodigoOrganizacao = codigocomposto;
                                                                                                                         bemterceiro.CentroCusto = codigocompostocc;
                                                                                                                         bemterceiro.Projeto = codigocompostopro;
                                                                                                                         bemterceiro.TipoBem = tipobem;

                                                                                                                         return bemterceiro;
                                                                                                                     }
                                                                                                                     , splitOn: "Id,Id,Id,CodigoTipoBem");

        }


        public BemTerceiro Get(int id)
        {
            return _connect.Query<BemTerceiro, CodigoComposto, CodigoComposto, CodigoComposto, TipoBem, BemTerceiro>($@"{_selectBemTerceiro}
                                                                                                                     and bt.bem_in_codigo = :id",
                                                                                                                     (bemterceiro, codigocomposto, codigocompostocc, codigocompostopro, tipobem) =>
                                                                                                                     {
                                                                                                                         bemterceiro.CodigoOrganizacao = codigocomposto;
                                                                                                                         bemterceiro.CentroCusto = codigocompostocc;
                                                                                                                         bemterceiro.Projeto = codigocompostopro;
                                                                                                                         bemterceiro.TipoBem = tipobem;

                                                                                                                         return bemterceiro;
                                                                                                                     },
                                                                                                                     new { id }
                                                                                                                     , splitOn: "Id,Id,Id,CodigoTipoBem").Single();
        }


        public IEnumerable<Avaliacao> GetAvaliacoes(int id)
        {
            return _connect.Query<Avaliacao>($@"{_selectAvaliacao}
                                                 where a.bem_in_codigo = :id",
                                                 new { id });
        }

        public IEnumerable<Despesa> GetDespesas(int id)
        {
            return _connect.Query<Despesa>($@"{_selectDespesa}
                                             where d.bem_in_codigo = :id",
                                             new { id });
        }

        public Contabilidade GetContabilidade(int id)
        {
            return _connect.Query<Contabilidade>($@"{_selectContabilidade}
                                                 where bec.bem_in_codigo = :id",
                                                 new[] { typeof(Contabilidade),
                                                         typeof(CodigoComposto), typeof(CodigoComposto), typeof(CodigoComposto), typeof(CodigoComposto), typeof(CodigoComposto),
                                                         typeof(CodigoComposto), typeof(CodigoComposto), typeof(CodigoComposto), typeof(CodigoComposto), typeof(CodigoComposto), typeof(CodigoComposto),
                                                         typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao),
                                                         typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao), typeof(CodigoAcao)
                                                 },
                                                 objects =>
                                                 
                                                 {
                                                     var contabilidade = objects[0] as Contabilidade;

                                                     var planocontascurtoprazo  = objects[1]  as CodigoComposto;
                                                     var planotransitoria       = objects[2]  as CodigoComposto;
                                                     var planojurosativos       = objects[3]  as CodigoComposto;
                                                     var planodistratoresultado = objects[4]  as CodigoComposto;
                                                     var planodistratopassivo   = objects[5]  as CodigoComposto;
                                                     var planorefvo             = objects[6]  as CodigoComposto;
                                                     var planorefvm             = objects[7]  as CodigoComposto;
                                                     var planopermuta           = objects[8]  as CodigoComposto;
                                                     var planocartacredito      = objects[9]  as CodigoComposto;
                                                     var planodacaopagamento    = objects[10] as CodigoComposto;
                                                     var planodireitosobrevenda = objects[11] as CodigoComposto;

                                                     var acaoinclusaocontrato    = objects[12] as CodigoAcao;
                                                     var acaotermocontratual     = objects[13] as CodigoAcao;
                                                     var acaomovimentobaixa      = objects[14] as CodigoAcao;
                                                     var acaodistratoapagar      = objects[15] as CodigoAcao;
                                                     var acaodistratosaldo       = objects[16] as CodigoAcao;
                                                     var acaoapropriacaojuros    = objects[17] as CodigoAcao;
                                                     var acaorenegociacaodebito  = objects[18] as CodigoAcao;
                                                     var acaorenegociacaocredito = objects[19] as CodigoAcao;
                                                     var acaocontratoaluguel     = objects[20] as CodigoAcao;
                                                     var acaopermuta             = objects[21] as CodigoAcao;
                                                     var acaocartacredito        = objects[22] as CodigoAcao;
                                                     var acaodacaodepagamento    = objects[23] as CodigoAcao;
                                                     var acaodireitosobrevenda   = objects[24] as CodigoAcao;
                                                     var acaotaxasadicionais     = objects[25] as CodigoAcao;
                                                     var acaonaocontabiliza      = objects[26] as CodigoAcao;
                                                     var acaodistratoapagarnc    = objects[27] as CodigoAcao;
                                                     var acaodistratotermoapagar = objects[28] as CodigoAcao;
                                                     var acaodistratotermosaldo  = objects[29] as CodigoAcao;


                                                     contabilidade.PlanoContasCurtoPrazo = planocontascurtoprazo;
                                                     contabilidade.PlanoTransitoria = planotransitoria;
                                                     contabilidade.PlanoJurosAtivos = planojurosativos;
                                                     contabilidade.PlanoDistratoResultado = planodistratoresultado;
                                                     contabilidade.PlanoDistratoPassivo = planodistratopassivo;
                                                     contabilidade.PlanoREFVO = planorefvo;
                                                     contabilidade.PlanoREFVM = planorefvm;
                                                     contabilidade.PlanoPermuta = planopermuta;
                                                     contabilidade.PlanoCartaCredito = planocartacredito;
                                                     contabilidade.PlanoDacaoPagamento = planodacaopagamento;
                                                     contabilidade.PlanoDireitoSobreVenda = planodireitosobrevenda;

                                                     contabilidade.AcaoInclusaoContrato = acaoinclusaocontrato;
                                                     contabilidade.AcaoTermoContratual = acaotermocontratual;
                                                     contabilidade.AcaoMovimentoBaixa = acaomovimentobaixa;
                                                     contabilidade.AcaoDistratoAPagar = acaodistratoapagar;
                                                     contabilidade.AcaoDistratoSaldo = acaodistratosaldo;
                                                     contabilidade.AcaoApropriacaoJuros = acaoapropriacaojuros;
                                                     contabilidade.AcaoRenegociacaoDebito = acaorenegociacaodebito;
                                                     contabilidade.AcaoRenegociacaoCredito = acaorenegociacaocredito;
                                                     contabilidade.AcaoContratoAluguel = acaocontratoaluguel;
                                                     contabilidade.AcaoPermuta = acaopermuta;
                                                     contabilidade.AcaoCartaCredito = acaocartacredito;
                                                     contabilidade.AcaoDacaoDePagamento = acaodacaodepagamento;
                                                     contabilidade.AcaoDireitoSobreVenda = acaodireitosobrevenda;
                                                     contabilidade.AcaoTaxasAdicionais = acaotaxasadicionais;
                                                     contabilidade.AcaoNaoContabiliza = acaonaocontabiliza;
                                                     contabilidade.AcaoDistratoAPagarNC = acaodistratoapagarnc;
                                                     contabilidade.AcaoDistratoTermoAPagar = acaodistratotermoapagar;
                                                     contabilidade.AcaoDistratoTermoSaldo = acaodistratotermosaldo;


                                                     return contabilidade;
                                                     },
                                                     new { id }).SingleOrDefault();
        }


        public BemTerceiro Insert(BemTerceiro bemterceiro)
        {
            _connect.Execute(@"insert into mgdbm.dbm_bens_terceiro(org_tab_in_codigo, org_pad_in_codigo, org_in_codigo, org_tau_st_codigo, bem_in_codigo,
                                                                         ind_in_codigo, tbe_in_codigo, bem_st_nome, bem_ch_status, bem_dt_status, bem_ch_quitacao, bem_bo_alienado,
                                                                         bem_re_imo_areatotal, bem_re_imo_areaconstruida, bem_in_imo_dependencias, bem_st_vei_placa, bem_re_valorvenda,
                                                                         bem_re_valorpretendido, bem_re_valorautorizado, bem_re_vei_kilometragem, bem_ch_vei_combustivel, bem_bl_observacoes,
                                                                         bem_re_utilizacao, bem_dt_cadastro, cus_tab_in_codigo, cus_pad_in_codigo, cus_ide_st_codigo, cus_in_reduzido,
                                                                         pro_tab_in_codigo, pro_pad_in_codigo, pro_ide_st_codigo, pro_in_reduzido, bem_re_valorutilizado, cnd_in_codigo,
                                                                         bemimob_st_codigo, natbem_in_codigo, bem_ch_baserecsped, bem_ch_participasped, bem_bo_rectermncontrat, bem_bo_bemgeraf500,
                                                                         bem_bo_consctosemrec, bem_ch_spedcstpis, bem_ch_spedcstcofins, bem_bo_spedconsideradesc, bem_bo_consencargossped)
                                                                  values(:org_tab_in_codigo, :org_pad_in_codigo, :org_in_codigo, :org_tau_st_codigo, :bem_in_codigo,
                                                                         :ind_in_codigo, :tbe_in_codigo, :bem_st_nome, chr(:bem_ch_status), :bem_dt_status, chr(:bem_ch_quitacao), chr(:bem_bo_alienado),
                                                                         :bem_re_imo_areatotal, :bem_re_imo_areaconstruida, :bem_in_imo_dependencias, :bem_st_vei_placa, :bem_re_valorvenda,
                                                                         :bem_re_valorpretendido, :bem_re_valorautorizado, :bem_re_vei_kilometragem, chr(:bem_ch_vei_combustivel), :bem_bl_observacoes,
                                                                         :bem_re_utilizacao, :bem_dt_cadastro, :cus_tab_in_codigo, :cus_pad_in_codigo, :cus_ide_st_codigo, :cus_in_reduzido,
                                                                         :pro_tab_in_codigo, :pro_pad_in_codigo, :pro_ide_st_codigo, :pro_in_reduzido, :bem_re_valorutilizado, :cnd_in_codigo,
                                                                         :bemimob_st_codigo, :natbem_in_codigo, chr(:bem_ch_baserecsped), chr(:bem_ch_participasped), chr(:bem_bo_rectermncontrat), chr(:bem_bo_bemgeraf500),
                                                                         chr(:bem_bo_consctosemrec), :bem_ch_spedcstpis, :bem_ch_spedcstcofins, chr(:bem_bo_spedconsideradesc), chr(:bem_bo_consencargossped)) 
                                                                       ", new
            {
                org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                org_in_codigo = bemterceiro.CodigoOrganizacao.Id,
                org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                bem_in_codigo = bemterceiro.Id,
                ind_in_codigo = bemterceiro.CodigoIndice,
                tbe_in_codigo = bemterceiro.TipoBem.CodigoTipoBem,
                bem_st_nome = bemterceiro.Nome,
                bem_ch_status = bemterceiro.Status,
                bem_dt_status = bemterceiro.DataStatus,
                bem_ch_quitacao = bemterceiro.Quitacao,
                bem_bo_alienado = bemterceiro.Alienado,
                bem_re_imo_areatotal = bemterceiro.TipoBem.AreaTotal,
                bem_re_imo_areaconstruida = bemterceiro.TipoBem.AreaConstruida,
                bem_in_imo_dependencias = bemterceiro.TipoBem.Dependencias,
                bem_st_vei_placa = bemterceiro.TipoBem.Placa,
                bem_re_valorvenda = bemterceiro.ValorVenda,
                bem_re_valorpretendido = bemterceiro.ValorPretendido,
                bem_re_valorautorizado = bemterceiro.ValorAutorizado,
                bem_re_vei_kilometragem = bemterceiro.TipoBem.Kilometragem,
                bem_ch_vei_combustivel = bemterceiro.TipoBem.Combustivel,
                bem_bl_observacoes = bemterceiro.Observacao,
                bem_re_utilizacao = bemterceiro.Utilizacao,
                bem_dt_cadastro = bemterceiro.DataCadastro,
                cus_tab_in_codigo = bemterceiro.CentroCusto.Tabela,
                cus_pad_in_codigo = bemterceiro.CentroCusto.Padrao,
                cus_ide_st_codigo = bemterceiro.CentroCusto.TipoIndetificador,
                cus_in_reduzido = bemterceiro.CentroCusto.Id,
                pro_tab_in_codigo = bemterceiro.Projeto.Tabela,
                pro_pad_in_codigo = bemterceiro.Projeto.Padrao,
                pro_ide_st_codigo = bemterceiro.Projeto.TipoIndetificador,
                pro_in_reduzido = bemterceiro.Projeto.Id,
                bem_re_valorutilizado = bemterceiro.ValorUtilizado, 
                cnd_in_codigo = bemterceiro.CodigoProposta,
                bemimob_st_codigo = bemterceiro.CodigoUnidadeImobiliaria, 
                natbem_in_codigo = bemterceiro.CodigoNaturezaEspecifica, 
                bem_ch_baserecsped = bemterceiro.BaseReceitaSPED, 
                bem_ch_participasped = bemterceiro.ParticipaSPEDPISCOFINS, 
                bem_bo_rectermncontrat = bemterceiro.ReceitasNaoContratuais, 
                bem_bo_bemgeraf500 = bemterceiro.GeraF500F525,
                bem_bo_consctosemrec = bemterceiro.ContratosSemRecebimento, 
                bem_ch_spedcstpis = bemterceiro.SituacaoTributariaPIS, 
                bem_ch_spedcstcofins = bemterceiro.SituacaoTributariaCOFINS, 
                bem_bo_spedconsideradesc = bemterceiro.DescontoPISCOFINS, 
                bem_bo_consencargossped = bemterceiro.Encargos
            });

            return bemterceiro;
        }

        public Avaliacao InsertAvaliacao(BemTerceiro bemterceiro, Avaliacao avaliacao)
        {
            _connect.Execute(@"insert into mgdbm.dbm_avaliacao(org_tab_in_codigo, org_pad_in_codigo, org_in_codigo, org_tau_st_codigo, bem_in_codigo, ava_in_codigo,  
                                                               ava_dt_avaliacao, ava_st_avaliador, ava_re_valormaximo, ava_re_valorminimo)
                                                        values(:org_tab_in_codigo, :org_pad_in_codigo, :org_in_codigo, :org_tau_st_codigo, :bem_in_codigo, :ava_in_codigo,  
                                                               :ava_dt_avaliacao, :ava_st_avaliador, :ava_re_valormaximo, :ava_re_valorminimo)",
                        new
                        {
                            org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                            org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                            org_in_codigo = bemterceiro.CodigoOrganizacao.Id,
                            org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                            bem_in_codigo = bemterceiro.Id,
                            ava_in_codigo = avaliacao.Id,
                            ava_dt_avaliacao = avaliacao.DataAvaliacao,
                            ava_st_avaliador = avaliacao.NomeAvaliador,
                            ava_re_valormaximo = avaliacao.ValorMaximo,
                            ava_re_valorminimo = avaliacao.ValorMinimo
                        });

            return avaliacao;
        }

        public Despesa InsertDespesa(BemTerceiro bemterceiro, Despesa despesa)
        {
            _connect.Execute(@"insert into mgdbm.dbm_despesa_bem(org_tab_in_codigo, org_pad_in_codigo, org_in_codigo, org_tau_st_codigo, bem_in_codigo, des_in_codigo,	
                                                                 des_re_valor, des_dt_cadastro, des_st_historico, des_bo_agrega)
                                                          values(:org_tab_in_codigo, :org_pad_in_codigo, :org_in_codigo, :org_tau_st_codigo, :bem_in_codigo, :des_in_codigo,	
                                                                 :des_re_valor, :des_dt_cadastro, :des_st_historico, chr(:des_bo_agrega))",
                        new
                        {
                            org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                            org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                            org_in_codigo = bemterceiro.CodigoOrganizacao.Id,
                            org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                            bem_in_codigo = bemterceiro.Id,
                            des_in_codigo = despesa.Id,
                            des_re_valor = despesa.Valor,
                            des_dt_cadastro = despesa.DataCadastro,
                            des_st_historico = despesa.Historico,
                            des_bo_agrega = despesa.Agrega
                        });

            return despesa;
        }

        public Contabilidade InsertContabilidade(BemTerceiro bemterceiro, Contabilidade contabilidade)
        {
            _connect.Execute(@"insert into mgdbm.dbm_bem_contabilidade(org_tab_in_codigo, org_pad_in_codigo, org_in_codigo, org_tau_st_codigo, bem_in_codigo, acao_tab_in_codigo,
                                                                       acaoe_pad_in_codigo, acaoe_in_codigo, acaoi_pad_in_codigo, acaoi_in_codigo, acaot_pad_in_codigo, acaot_in_codigo,
                                                                       acaog_pad_in_codigo, acaog_in_codigo, acaov_pad_in_codigo, acaov_in_codigo, acaod_pad_in_codigo, acaod_in_codigo,
                                                                       acaop_pad_in_codigo, acaop_in_codigo, acaom_pad_in_codigo, acaom_in_codigo, acaol_pad_in_codigo, acaol_in_codigo,
                                                                       acaoa_pad_in_codigo, acaoa_in_codigo, acaos_pad_in_codigo, acaos_in_codigo, acaob_pad_in_codigo, acaob_in_codigo,
                                                                       pla_tab_in_codigo, pla_pad_in_codigo, clicp_pla_ide_st_codigo, clicp_pla_in_reduzido, disp_pla_ide_st_codigo, disp_pla_in_reduzido,
                                                                       vlrori_pla_ide_st_codigo, vlrori_pla_in_reduzido, trans_pla_ide_st_codigo, trans_pla_in_reduzido, per_pla_ide_st_codigo, per_pla_in_reduzido,
                                                                       bem_pla_ide_st_codigo, bem_pla_in_reduzido, /*clilp_pla_ide_st_codigo, clilp_pla_in_reduzido,*/ jrocto_pla_ide_st_codigo, jrocto_pla_in_reduzido,  
                                                                       dis_pla_ide_st_codigo, dis_pla_in_reduzido, varmon_pla_ide_st_codigo, varmon_pla_in_reduzido, /*acaon_pad_in_codigo, acaon_in_codigo,*/  
                                                                       /*acaox_pad_in_codigo, acaox_in_codigo,*/ /*acaor_pad_in_codigo, acaor_in_codigo,*/ /*acaodc_pad_in_codigo, acaodc_in_codigo,*/ /*acaoxv_pad_in_codigo, acaoxv_in_codigo,*/
                                                                       /*acaoxg_pad_in_codigo, acaoxg_in_codigo,*/ /*acaoxp_pad_in_codigo, acaoxp_in_codigo,*/ /*acaorv_pad_in_codigo, acaorv_in_codigo,*/ /*acaorg_pad_in_codigo, acaorg_in_codigo,*/  
                                                                       /*acaorp_pad_in_codigo, acaorp_in_codigo,*/ ven_pla_ide_st_codigo, ven_pla_in_reduzido, car_pla_ide_st_codigo, car_pla_in_reduzido, /*acaork_pad_in_codigo, acaork_in_codigo,*/  
                                                                       acaok_pad_in_codigo, acaok_in_codigo, acaotx_pad_in_codigo, acaotx_in_codigo, acaonc_tab_in_codigo, acaonc_pad_in_codigo, acaonc_in_codigo, acaodt_tab_in_codigo, acaodt_pad_in_codigo, acaodt_in_codigo,  
                                                                       acaoet_tab_in_codigo, acaoet_pad_in_codigo, acaoet_in_codigo, acaodtn_tab_in_codigo, acaodtn_pad_in_codigo, acaodtn_in_codigo/*, acaoxk_tab_in_codigo, acaoxk_pad_in_codigo, acaoxk_in_codigo*/)
                                                                values(:org_tab_in_codigo, :org_pad_in_codigo, :org_in_codigo, :org_tau_st_codigo, :bem_in_codigo, :acao_tab_in_codigo,
                                                                       :acaoe_pad_in_codigo, :acaoe_in_codigo, :acaoi_pad_in_codigo, :acaoi_in_codigo, :acaot_pad_in_codigo, :acaot_in_codigo,
                                                                       :acaog_pad_in_codigo, :acaog_in_codigo, :acaov_pad_in_codigo, :acaov_in_codigo, :acaod_pad_in_codigo, :acaod_in_codigo,
                                                                       :acaop_pad_in_codigo, :acaop_in_codigo, :acaom_pad_in_codigo, :acaom_in_codigo, :acaol_pad_in_codigo, :acaol_in_codigo,
                                                                       :acaoa_pad_in_codigo, :acaoa_in_codigo, :acaos_pad_in_codigo, :acaos_in_codigo, :acaob_pad_in_codigo, :acaob_in_codigo,
                                                                       :pla_tab_in_codigo, :pla_pad_in_codigo, :clicp_pla_ide_st_codigo, :clicp_pla_in_reduzido, :disp_pla_ide_st_codigo, :disp_pla_in_reduzido,
                                                                       :vlrori_pla_ide_st_codigo, :vlrori_pla_in_reduzido, :trans_pla_ide_st_codigo, :trans_pla_in_reduzido, :per_pla_ide_st_codigo, :per_pla_in_reduzido,
                                                                       :bem_pla_ide_st_codigo, :bem_pla_in_reduzido, /*:clilp_pla_ide_st_codigo, :clilp_pla_in_reduzido,*/ :jrocto_pla_ide_st_codigo, :jrocto_pla_in_reduzido,  
                                                                       :dis_pla_ide_st_codigo, :dis_pla_in_reduzido, :varmon_pla_ide_st_codigo, :varmon_pla_in_reduzido, /*:acaon_pad_in_codigo, :acaon_in_codigo,*/  
                                                                       /*:acaox_pad_in_codigo, :acaox_in_codigo,*/ /*:acaor_pad_in_codigo, :acaor_in_codigo,*/ /*:acaodc_pad_in_codigo, :acaodc_in_codigo,*/ /*:acaoxv_pad_in_codigo, :acaoxv_in_codigo,*/
                                                                       /*:acaoxg_pad_in_codigo, :acaoxg_in_codigo,*/ /*:acaoxp_pad_in_codigo, :acaoxp_in_codigo,*/ /*:acaorv_pad_in_codigo, :acaorv_in_codigo,*/ /*:acaorg_pad_in_codigo, :acaorg_in_codigo, */ 
                                                                       /*:acaorp_pad_in_codigo, :acaorp_in_codigo,*/ :ven_pla_ide_st_codigo, :ven_pla_in_reduzido, :car_pla_ide_st_codigo, :car_pla_in_reduzido, /*:acaork_pad_in_codigo, :acaork_in_codigo,*/  
                                                                       :acaok_pad_in_codigo, :acaok_in_codigo, :acaotx_pad_in_codigo, :acaotx_in_codigo, :acaonc_tab_in_codigo, :acaonc_pad_in_codigo, :acaonc_in_codigo, :acaodt_tab_in_codigo, :acaodt_pad_in_codigo, :acaodt_in_codigo,  
                                                                       :acaoet_tab_in_codigo, :acaoet_pad_in_codigo, :acaoet_in_codigo, :acaodtn_tab_in_codigo, :acaodtn_pad_in_codigo, :acaodtn_in_codigo/*, :acaoxk_tab_in_codigo, :acaoxk_pad_in_codigo, :acaoxk_in_codigo*/)",
                        new
                        {
                            org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                            org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                            org_in_codigo = bemterceiro.CodigoOrganizacao.Id,
                            org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                            bem_in_codigo = bemterceiro.Id,

                            clicp_pla_in_reduzido = contabilidade.PlanoContasCurtoPrazo.Id,
                            pla_tab_in_codigo = contabilidade.PlanoContasCurtoPrazo.Tabela,
                            pla_pad_in_codigo = contabilidade.PlanoContasCurtoPrazo.Padrao,
                            clicp_pla_ide_st_codigo = contabilidade.PlanoContasCurtoPrazo.TipoIndetificador,

                            trans_pla_in_reduzido = contabilidade.PlanoTransitoria.Id,
                            trans_pla_ide_st_codigo = contabilidade.PlanoTransitoria.TipoIndetificador,

                            jrocto_pla_in_reduzido = contabilidade.PlanoJurosAtivos.Id,
                            jrocto_pla_ide_st_codigo = contabilidade.PlanoJurosAtivos.TipoIndetificador,

                            dis_pla_in_reduzido = contabilidade.PlanoDistratoResultado.Id,
                            dis_pla_ide_st_codigo = contabilidade.PlanoDistratoResultado.TipoIndetificador,

                            disp_pla_in_reduzido = contabilidade.PlanoDistratoPassivo.Id,
                            disp_pla_ide_st_codigo = contabilidade.PlanoDistratoPassivo.TipoIndetificador,

                            vlrori_pla_in_reduzido = contabilidade.PlanoREFVO.Id,
                            vlrori_pla_ide_st_codigo = contabilidade.PlanoREFVO.TipoIndetificador,

                            varmon_pla_in_reduzido = contabilidade.PlanoREFVM.Id,
                            varmon_pla_ide_st_codigo = contabilidade.PlanoREFVM.TipoIndetificador,

                            per_pla_in_reduzido = contabilidade.PlanoPermuta.Id,
                            per_pla_ide_st_codigo = contabilidade.PlanoPermuta.TipoIndetificador,

                            car_pla_in_reduzido = contabilidade.PlanoCartaCredito.Id,
                            car_pla_ide_st_codigo = contabilidade.PlanoCartaCredito.TipoIndetificador,

                            bem_pla_in_reduzido = contabilidade.PlanoDacaoPagamento.Id,
                            bem_pla_ide_st_codigo = contabilidade.PlanoDacaoPagamento.TipoIndetificador,

                            ven_pla_in_reduzido = contabilidade.PlanoDireitoSobreVenda.Id,
                            ven_pla_ide_st_codigo = contabilidade.PlanoDireitoSobreVenda.TipoIndetificador,

                            acaoi_in_codigo = contabilidade.AcaoInclusaoContrato.Id,
                            acao_tab_in_codigo = contabilidade.AcaoInclusaoContrato.Tabela,
                            acaoi_pad_in_codigo = contabilidade.AcaoInclusaoContrato.Padrao,
                            

                            acaot_in_codigo = contabilidade.AcaoTermoContratual.Id,
                            acaot_pad_in_codigo = contabilidade.AcaoTermoContratual.Padrao,

                            acaom_in_codigo = contabilidade.AcaoMovimentoBaixa.Id,
                            acaom_pad_in_codigo = contabilidade.AcaoMovimentoBaixa.Padrao,

                            acaod_in_codigo = contabilidade.AcaoDistratoAPagar.Id,
                            acaod_pad_in_codigo = contabilidade.AcaoDistratoAPagar.Padrao,

                            acaoe_in_codigo = contabilidade.AcaoDistratoSaldo.Id,
                            acaoe_pad_in_codigo = contabilidade.AcaoDistratoSaldo.Padrao,

                            acaos_in_codigo = contabilidade.AcaoApropriacaoJuros.Id,
                            acaos_pad_in_codigo = contabilidade.AcaoApropriacaoJuros.Padrao,

                            acaob_in_codigo = contabilidade.AcaoRenegociacaoDebito.Id,
                            acaob_pad_in_codigo = contabilidade.AcaoRenegociacaoDebito.Padrao,

                            acaoa_in_codigo = contabilidade.AcaoRenegociacaoCredito.Id,
                            acaoa_pad_in_codigo = contabilidade.AcaoRenegociacaoCredito.Padrao,

                            acaol_in_codigo = contabilidade.AcaoContratoAluguel.Id,
                            acaol_pad_in_codigo = contabilidade.AcaoContratoAluguel.Padrao,

                            acaop_in_codigo = contabilidade.AcaoPermuta.Id,
                            acaop_pad_in_codigo = contabilidade.AcaoPermuta.Padrao,

                            acaok_in_codigo = contabilidade.AcaoCartaCredito.Id,
                            acaok_pad_in_codigo = contabilidade.AcaoCartaCredito.Padrao,

                            acaog_in_codigo = contabilidade.AcaoDacaoDePagamento.Id,
                            acaog_pad_in_codigo = contabilidade.AcaoDacaoDePagamento.Padrao,

                            acaov_in_codigo = contabilidade.AcaoDireitoSobreVenda.Id,
                            acaov_pad_in_codigo = contabilidade.AcaoDireitoSobreVenda.Padrao,

                            acaotx_in_codigo = contabilidade.AcaoTaxasAdicionais.Id,
                            acaotx_pad_in_codigo = contabilidade.AcaoTaxasAdicionais.Padrao,

                            acaonc_in_codigo = contabilidade.AcaoNaoContabiliza.Id,
                            acaonc_tab_in_codigo = contabilidade.AcaoNaoContabiliza.Tabela,
                            acaonc_pad_in_codigo = contabilidade.AcaoNaoContabiliza.Padrao,

                            acaodtn_in_codigo = contabilidade.AcaoDistratoAPagarNC.Id,
                            acaodtn_tab_in_codigo = contabilidade.AcaoDistratoAPagarNC.Tabela,
                            acaodtn_pad_in_codigo = contabilidade.AcaoDistratoAPagarNC.Padrao,

                            acaodt_in_codigo = contabilidade.AcaoDistratoTermoAPagar.Id,
                            acaodt_tab_in_codigo = contabilidade.AcaoDistratoTermoAPagar.Tabela,
                            acaodt_pad_in_codigo = contabilidade.AcaoDistratoTermoAPagar.Padrao,

                            acaoet_tab_in_codigo = contabilidade.AcaoDistratoTermoSaldo.Id,
                            acaoet_pad_in_codigo = contabilidade.AcaoDistratoTermoSaldo.Tabela,
                            acaoet_in_codigo = contabilidade.AcaoDistratoTermoSaldo.Padrao

                        });
            return contabilidade;
        }

        public BemTerceiro Update(BemTerceiro bemterceiro)
        {
            _connect.Execute(@"update mgdbm.dbm_bens_terceiro
                                  set ind_in_codigo = :ind_in_codigo,
                                      tbe_in_codigo = :tbe_in_codigo,
                                      bem_st_nome = :bem_st_nome,  
                                      bem_ch_status = chr(:bem_ch_status),
                                      bem_dt_status = :bem_dt_status,
                                      bem_ch_quitacao = chr(:bem_ch_quitacao),
                                      bem_bo_alienado = chr(:bem_bo_alienado),  
                                      bem_re_imo_areatotal = :bem_re_imo_areatotal,
                                      bem_re_imo_areaconstruida = :bem_re_imo_areaconstruida,
                                      bem_in_imo_dependencias = :bem_in_imo_dependencias,
                                      bem_st_vei_placa = :bem_st_vei_placa,
                                      bem_re_valorvenda = :bem_re_valorvenda,
                                      bem_re_valorpretendido = :bem_re_valorpretendido,
                                      bem_re_valorautorizado = :bem_re_valorautorizado,
                                      bem_re_vei_kilometragem = :bem_re_vei_kilometragem,
                                      bem_ch_vei_combustivel = chr(:bem_ch_vei_combustivel),
                                      bem_bl_observacoes = :bem_bl_observacoes,
                                      bem_re_utilizacao = :bem_re_utilizacao,
                                      bem_dt_cadastro = :bem_dt_cadastro,
                                      cus_tab_in_codigo = :cus_tab_in_codigo, 
                                      cus_pad_in_codigo = :cus_pad_in_codigo,
                                      cus_ide_st_codigo = :cus_ide_st_codigo,
                                      cus_in_reduzido = :cus_in_reduzido,
                                      pro_tab_in_codigo = :pro_tab_in_codigo,
                                      pro_pad_in_codigo = :pro_pad_in_codigo,
                                      pro_ide_st_codigo = :pro_ide_st_codigo,
                                      pro_in_reduzido = :pro_in_reduzido,
                                      bem_re_valorutilizado = :bem_re_valorutilizado,
                                      cnd_in_codigo = :cnd_in_codigo,
                                      bemimob_st_codigo = :bemimob_st_codigo,
                                      natbem_in_codigo = :natbem_in_codigo,
                                      bem_ch_baserecsped = chr(:bem_ch_baserecsped),
                                      bem_ch_participasped = chr(:bem_ch_participasped),
                                      bem_bo_rectermncontrat = chr(:bem_bo_rectermncontrat),
                                      bem_bo_bemgeraf500 = chr(:bem_bo_bemgeraf500),
                                      bem_bo_consctosemrec = chr(:bem_bo_consctosemrec),
                                      bem_ch_spedcstpis = :bem_ch_spedcstpis,
                                      bem_ch_spedcstcofins = :bem_ch_spedcstcofins,
                                      bem_bo_spedconsideradesc = chr(:bem_bo_spedconsideradesc),
                                      bem_bo_consencargossped = chr(:bem_bo_consencargossped)
                                where org_tab_in_codigo = :org_tab_in_codigo
                                  and org_pad_in_codigo = :org_pad_in_codigo
                                  and org_in_codigo = :org_in_codigo
                                  and org_tau_st_codigo = :org_tau_st_codigo
                                  and bem_in_codigo = :bem_in_codigo", 
             new
            {
                org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                org_in_codigo = bemterceiro.CodigoOrganizacao.Id,
                org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                bem_in_codigo = bemterceiro.Id,
                ind_in_codigo = bemterceiro.CodigoIndice,
                tbe_in_codigo = bemterceiro.TipoBem.CodigoTipoBem,
                bem_st_nome = bemterceiro.Nome,
                bem_ch_status = bemterceiro.Status,
                bem_dt_status = bemterceiro.DataStatus,
                bem_ch_quitacao = bemterceiro.Quitacao,
                bem_bo_alienado = bemterceiro.Alienado,
                bem_re_imo_areatotal = bemterceiro.TipoBem.AreaTotal,
                bem_re_imo_areaconstruida = bemterceiro.TipoBem.AreaConstruida,
                bem_in_imo_dependencias = bemterceiro.TipoBem.Dependencias,
                bem_st_vei_placa = bemterceiro.TipoBem.Placa,
                bem_re_valorvenda = bemterceiro.ValorVenda,
                bem_re_valorpretendido = bemterceiro.ValorPretendido,
                bem_re_valorautorizado = bemterceiro.ValorAutorizado,
                bem_re_vei_kilometragem = bemterceiro.TipoBem.Kilometragem,
                bem_ch_vei_combustivel = bemterceiro.TipoBem.Combustivel,
                bem_bl_observacoes = bemterceiro.Observacao,
                bem_re_utilizacao = bemterceiro.Utilizacao,
                bem_dt_cadastro = bemterceiro.DataCadastro,
                cus_tab_in_codigo = bemterceiro.CentroCusto.Tabela,
                cus_pad_in_codigo = bemterceiro.CentroCusto.Padrao,
                cus_ide_st_codigo = bemterceiro.CentroCusto.TipoIndetificador,
                cus_in_reduzido = bemterceiro.CentroCusto.Id,
                pro_tab_in_codigo = bemterceiro.Projeto.Tabela,
                pro_pad_in_codigo = bemterceiro.Projeto.Padrao,
                pro_ide_st_codigo = bemterceiro.Projeto.TipoIndetificador,
                pro_in_reduzido = bemterceiro.Projeto.Id,
                bem_re_valorutilizado = bemterceiro.ValorUtilizado,
                cnd_in_codigo = bemterceiro.CodigoProposta,
                bemimob_st_codigo = bemterceiro.CodigoUnidadeImobiliaria,
                natbem_in_codigo = bemterceiro.CodigoNaturezaEspecifica,
                bem_ch_baserecsped = bemterceiro.BaseReceitaSPED,
                bem_ch_participasped = bemterceiro.ParticipaSPEDPISCOFINS,
                bem_bo_rectermncontrat = bemterceiro.ReceitasNaoContratuais,
                bem_bo_bemgeraf500 = bemterceiro.GeraF500F525,
                bem_bo_consctosemrec = bemterceiro.ContratosSemRecebimento,
                bem_ch_spedcstpis = bemterceiro.SituacaoTributariaPIS,
                bem_ch_spedcstcofins = bemterceiro.SituacaoTributariaCOFINS,
                bem_bo_spedconsideradesc = bemterceiro.DescontoPISCOFINS,
                bem_bo_consencargossped = bemterceiro.Encargos
            });

            return bemterceiro;
        }

        public Contabilidade UpdateContabilidade(BemTerceiro bemterceiro, Contabilidade contabilidade)
        {
            _connect.Execute(@"update mgdbm.dbm_bem_contabilidade
                                  set acao_tab_in_codigo = :acao_tab_in_codigo,
                                      acaoe_pad_in_codigo = :acaoe_pad_in_codigo,
                                      acaoe_in_codigo = :acaoe_in_codigo,
                                      acaoi_pad_in_codigo = :acaoi_pad_in_codigo,
                                      acaoi_in_codigo = :acaoi_in_codigo,
                                      acaot_pad_in_codigo = :acaot_pad_in_codigo,
                                      acaot_in_codigo = :acaot_in_codigo,
                                      acaog_pad_in_codigo = :acaog_pad_in_codigo,
                                      acaog_in_codigo = :acaog_in_codigo, 
                                      acaov_pad_in_codigo = :acaov_pad_in_codigo, 
                                      acaov_in_codigo = :acaov_in_codigo, 
                                      acaod_pad_in_codigo = :acaod_pad_in_codigo, 
                                      acaod_in_codigo = :acaod_in_codigo,
                                      acaop_pad_in_codigo = :acaop_pad_in_codigo, 
                                      acaop_in_codigo = :acaop_in_codigo, 
                                      acaom_pad_in_codigo = :acaom_pad_in_codigo, 
                                      acaom_in_codigo = :acaom_in_codigo, 
                                      acaol_pad_in_codigo = :acaol_pad_in_codigo, 
                                      acaol_in_codigo = :acaol_in_codigo,
                                      acaoa_pad_in_codigo = :acaoa_pad_in_codigo, 
                                      acaoa_in_codigo = :acaoa_in_codigo, 
                                      acaos_pad_in_codigo = :acaos_pad_in_codigo, 
                                      acaos_in_codigo = :acaos_in_codigo, 
                                      acaob_pad_in_codigo = :acaob_pad_in_codigo, 
                                      acaob_in_codigo = :acaob_in_codigo,
                                      pla_tab_in_codigo = :pla_tab_in_codigo, 
                                      pla_pad_in_codigo = :pla_pad_in_codigo, 
                                      clicp_pla_ide_st_codigo = :clicp_pla_ide_st_codigo, 
                                      clicp_pla_in_reduzido = :clicp_pla_in_reduzido, 
                                      disp_pla_ide_st_codigo = :disp_pla_ide_st_codigo, 
                                      disp_pla_in_reduzido = :disp_pla_in_reduzido,
                                      vlrori_pla_ide_st_codigo = :vlrori_pla_ide_st_codigo, 
                                      vlrori_pla_in_reduzido = :vlrori_pla_in_reduzido, 
                                      trans_pla_ide_st_codigo = :trans_pla_ide_st_codigo, 
                                      trans_pla_in_reduzido = :trans_pla_in_reduzido, 
                                      per_pla_ide_st_codigo = :per_pla_ide_st_codigo, 
                                      per_pla_in_reduzido = :per_pla_in_reduzido,
                                      bem_pla_ide_st_codigo = :bem_pla_ide_st_codigo, 
                                      bem_pla_in_reduzido = :bem_pla_in_reduzido, 
                                      jrocto_pla_ide_st_codigo = :jrocto_pla_ide_st_codigo, 
                                      jrocto_pla_in_reduzido = :jrocto_pla_in_reduzido,  
                                      dis_pla_ide_st_codigo = :dis_pla_ide_st_codigo, 
                                      dis_pla_in_reduzido = :dis_pla_in_reduzido, 
                                      varmon_pla_ide_st_codigo = :varmon_pla_ide_st_codigo,
                                      varmon_pla_in_reduzido = :varmon_pla_in_reduzido, 
                                      ven_pla_ide_st_codigo = :ven_pla_ide_st_codigo, 
                                      ven_pla_in_reduzido = :ven_pla_in_reduzido, 
                                      car_pla_ide_st_codigo = :car_pla_ide_st_codigo, 
                                      car_pla_in_reduzido = :car_pla_in_reduzido, 
                                      acaok_pad_in_codigo = :acaok_pad_in_codigo, 
                                      acaok_in_codigo = :acaok_in_codigo, 
                                      acaotx_pad_in_codigo = :acaotx_pad_in_codigo, 
                                      acaotx_in_codigo = :acaotx_in_codigo, 
                                      acaonc_tab_in_codigo = :acaonc_tab_in_codigo, 
                                      acaonc_pad_in_codigo = :acaonc_pad_in_codigo, 
                                      acaonc_in_codigo = :acaonc_in_codigo, 
                                      acaodt_tab_in_codigo = :acaodt_tab_in_codigo, 
                                      acaodt_pad_in_codigo = :acaodt_pad_in_codigo, 
                                      acaodt_in_codigo = :acaodt_in_codigo, 
                                      acaoet_tab_in_codigo = :acaoet_tab_in_codigo,
                                      acaoet_pad_in_codigo = :acaoet_pad_in_codigo, 
                                      acaoet_in_codigo = :acaoet_in_codigo, 
                                      acaodtn_tab_in_codigo = :acaodtn_tab_in_codigo, 
                                      acaodtn_pad_in_codigo = :acaodtn_pad_in_codigo, 
                                      acaodtn_in_codigo = :acaodtn_in_codigo

                                where org_tab_in_codigo = :org_tab_in_codigo
                                  and org_pad_in_codigo = :org_pad_in_codigo
                                  and org_in_codigo = :org_in_codigo
                                  and org_tau_st_codigo = :org_tau_st_codigo
                                  and bem_in_codigo = :bem_in_codigo",
                        new
                        {
                            org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                            org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                            org_in_codigo = bemterceiro.CodigoOrganizacao.Id,
                            org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                            bem_in_codigo = bemterceiro.Id,

                            clicp_pla_in_reduzido = contabilidade.PlanoContasCurtoPrazo.Id,
                            pla_tab_in_codigo = contabilidade.PlanoContasCurtoPrazo.Tabela,
                            pla_pad_in_codigo = contabilidade.PlanoContasCurtoPrazo.Padrao,
                            clicp_pla_ide_st_codigo = contabilidade.PlanoContasCurtoPrazo.TipoIndetificador,

                            trans_pla_in_reduzido = contabilidade.PlanoTransitoria.Id,
                            trans_pla_ide_st_codigo = contabilidade.PlanoTransitoria.TipoIndetificador,

                            jrocto_pla_in_reduzido = contabilidade.PlanoJurosAtivos.Id,
                            jrocto_pla_ide_st_codigo = contabilidade.PlanoJurosAtivos.TipoIndetificador,

                            dis_pla_in_reduzido = contabilidade.PlanoDistratoResultado.Id,
                            dis_pla_ide_st_codigo = contabilidade.PlanoDistratoResultado.TipoIndetificador,

                            disp_pla_in_reduzido = contabilidade.PlanoDistratoPassivo.Id,
                            disp_pla_ide_st_codigo = contabilidade.PlanoDistratoPassivo.TipoIndetificador,

                            vlrori_pla_in_reduzido = contabilidade.PlanoREFVO.Id,
                            vlrori_pla_ide_st_codigo = contabilidade.PlanoREFVO.TipoIndetificador,

                            varmon_pla_in_reduzido = contabilidade.PlanoREFVM.Id,
                            varmon_pla_ide_st_codigo = contabilidade.PlanoREFVM.TipoIndetificador,

                            per_pla_in_reduzido = contabilidade.PlanoPermuta.Id,
                            per_pla_ide_st_codigo = contabilidade.PlanoPermuta.TipoIndetificador,

                            car_pla_in_reduzido = contabilidade.PlanoCartaCredito.Id,
                            car_pla_ide_st_codigo = contabilidade.PlanoCartaCredito.TipoIndetificador,

                            bem_pla_in_reduzido = contabilidade.PlanoDacaoPagamento.Id,
                            bem_pla_ide_st_codigo = contabilidade.PlanoDacaoPagamento.TipoIndetificador,

                            ven_pla_in_reduzido = contabilidade.PlanoDireitoSobreVenda.Id,
                            ven_pla_ide_st_codigo = contabilidade.PlanoDireitoSobreVenda.TipoIndetificador,

                            acaoi_in_codigo = contabilidade.AcaoInclusaoContrato.Id,
                            acao_tab_in_codigo = contabilidade.AcaoInclusaoContrato.Tabela,
                            acaoi_pad_in_codigo = contabilidade.AcaoInclusaoContrato.Padrao,


                            acaot_in_codigo = contabilidade.AcaoTermoContratual.Id,
                            acaot_pad_in_codigo = contabilidade.AcaoTermoContratual.Padrao,

                            acaom_in_codigo = contabilidade.AcaoMovimentoBaixa.Id,
                            acaom_pad_in_codigo = contabilidade.AcaoMovimentoBaixa.Padrao,

                            acaod_in_codigo = contabilidade.AcaoDistratoAPagar.Id,
                            acaod_pad_in_codigo = contabilidade.AcaoDistratoAPagar.Padrao,

                            acaoe_in_codigo = contabilidade.AcaoDistratoSaldo.Id,
                            acaoe_pad_in_codigo = contabilidade.AcaoDistratoSaldo.Padrao,

                            acaos_in_codigo = contabilidade.AcaoApropriacaoJuros.Id,
                            acaos_pad_in_codigo = contabilidade.AcaoApropriacaoJuros.Padrao,

                            acaob_in_codigo = contabilidade.AcaoRenegociacaoDebito.Id,
                            acaob_pad_in_codigo = contabilidade.AcaoRenegociacaoDebito.Padrao,

                            acaoa_in_codigo = contabilidade.AcaoRenegociacaoCredito.Id,
                            acaoa_pad_in_codigo = contabilidade.AcaoRenegociacaoCredito.Padrao,

                            acaol_in_codigo = contabilidade.AcaoContratoAluguel.Id,
                            acaol_pad_in_codigo = contabilidade.AcaoContratoAluguel.Padrao,

                            acaop_in_codigo = contabilidade.AcaoPermuta.Id,
                            acaop_pad_in_codigo = contabilidade.AcaoPermuta.Padrao,

                            acaok_in_codigo = contabilidade.AcaoCartaCredito.Id,
                            acaok_pad_in_codigo = contabilidade.AcaoCartaCredito.Padrao,

                            acaog_in_codigo = contabilidade.AcaoDacaoDePagamento.Id,
                            acaog_pad_in_codigo = contabilidade.AcaoDacaoDePagamento.Padrao,

                            acaov_in_codigo = contabilidade.AcaoDireitoSobreVenda.Id,
                            acaov_pad_in_codigo = contabilidade.AcaoDireitoSobreVenda.Padrao,

                            acaotx_in_codigo = contabilidade.AcaoTaxasAdicionais.Id,
                            acaotx_pad_in_codigo = contabilidade.AcaoTaxasAdicionais.Padrao,

                            acaonc_in_codigo = contabilidade.AcaoNaoContabiliza.Id,
                            acaonc_tab_in_codigo = contabilidade.AcaoNaoContabiliza.Tabela,
                            acaonc_pad_in_codigo = contabilidade.AcaoNaoContabiliza.Padrao,

                            acaodtn_in_codigo = contabilidade.AcaoDistratoAPagarNC.Id,
                            acaodtn_tab_in_codigo = contabilidade.AcaoDistratoAPagarNC.Tabela,
                            acaodtn_pad_in_codigo = contabilidade.AcaoDistratoAPagarNC.Padrao,

                            acaodt_in_codigo = contabilidade.AcaoDistratoTermoAPagar.Id,
                            acaodt_tab_in_codigo = contabilidade.AcaoDistratoTermoAPagar.Tabela,
                            acaodt_pad_in_codigo = contabilidade.AcaoDistratoTermoAPagar.Padrao,

                            acaoet_tab_in_codigo = contabilidade.AcaoDistratoTermoSaldo.Id,
                            acaoet_pad_in_codigo = contabilidade.AcaoDistratoTermoSaldo.Tabela,
                            acaoet_in_codigo = contabilidade.AcaoDistratoTermoSaldo.Padrao

                        });
            return contabilidade;
        }


        public BemTerceiro Delete(BemTerceiro bemterceiro)
        {
            _connect.Execute(@"delete mgdbm.dbm_bens_terceiro
                                where org_tab_in_codigo = :org_tab_in_codigo
                                  and org_pad_in_codigo = :org_pad_in_codigo
                                  and org_in_codigo = :org_in_codigo
                                  and org_tau_st_codigo = :org_tau_st_codigo
                                  and bem_in_codigo = :bem_in_codigo"
                , new { org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                        org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                        org_in_codigo     = bemterceiro.CodigoOrganizacao.Id,
                        org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                        bem_in_codigo     = bemterceiro.Id });

            return bemterceiro;
        }

        public Avaliacao DeleteAvaliacao(BemTerceiro bemterceiro, Avaliacao avaliacao)
        {
            _connect.Execute(@"delete mgdbm.dbm_avaliacao
                                where org_tab_in_codigo = :org_tab_in_codigo
                                  and org_pad_in_codigo = :org_pad_in_codigo
                                  and org_in_codigo = :org_in_codigo
                                  and org_tau_st_codigo = :org_tau_st_codigo
                                  and bem_in_codigo = :bem_in_codigo
                                  and ava_in_codigo = :ava_in_codigo"
                , new
                {
                    org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                    org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                    org_in_codigo     = bemterceiro.CodigoOrganizacao.Id,
                    org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                    bem_in_codigo     = bemterceiro.Id,
                    ava_in_codigo     = avaliacao.Id
                });
            return avaliacao;
        }

        public Despesa DeleteDespesa(BemTerceiro bemterceiro, Despesa despesa)
        {
            _connect.Execute(@"delete mgdbm.dbm_despesa_bem
                                where org_tab_in_codigo = :org_tab_in_codigo
                                  and org_pad_in_codigo = :org_pad_in_codigo
                                  and org_in_codigo = :org_in_codigo
                                  and org_tau_st_codigo = :org_tau_st_codigo
                                  and bem_in_codigo = :bem_in_codigo
                                  and des_in_codigo = :des_in_codigo"
                , new
                {
                    org_tab_in_codigo = bemterceiro.CodigoOrganizacao.Tabela,
                    org_pad_in_codigo = bemterceiro.CodigoOrganizacao.Padrao,
                    org_in_codigo     = bemterceiro.CodigoOrganizacao.Id,
                    org_tau_st_codigo = bemterceiro.CodigoOrganizacao.TipoIndetificador,
                    bem_in_codigo     = bemterceiro.Id,
                    des_in_codigo     = despesa.Id
                });
            return despesa;
        }


    }
}
