﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.GlobalDBM.BensTerceiros.Models;


namespace Mega.Erp.GlobalDBM.BensTerceiros.Interfaces
{
    public interface IBemTerceiroRepository
    {
        IEnumerable<BemTerceiro> List();
        BemTerceiro Get(int id);
        IEnumerable<Avaliacao> GetAvaliacoes(int id);
        IEnumerable<Despesa> GetDespesas(int id);
        Contabilidade GetContabilidade(int id);
        BemTerceiro Insert(BemTerceiro bemterceiro);
        Avaliacao InsertAvaliacao(BemTerceiro bemterceiro, Avaliacao avaliacao);
        Despesa InsertDespesa(BemTerceiro bemterceiro, Despesa despesa);
        Contabilidade InsertContabilidade(BemTerceiro bemterceiro, Contabilidade contabilidade);
        BemTerceiro Update(BemTerceiro bemterceiro);
        Contabilidade UpdateContabilidade(BemTerceiro bemterceiro, Contabilidade contabilidade);
        BemTerceiro Delete(BemTerceiro bemterceiro);
        Avaliacao DeleteAvaliacao(BemTerceiro bemterceiro, Avaliacao avaliacao);
        Despesa DeleteDespesa(BemTerceiro bemterceiro, Despesa despesa);

    }
}
