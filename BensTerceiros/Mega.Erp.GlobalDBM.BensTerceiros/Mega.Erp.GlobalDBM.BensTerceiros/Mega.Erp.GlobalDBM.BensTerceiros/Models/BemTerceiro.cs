﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Models
{
    public enum TipoStatus
    {
        Disponível = 'D',
        Indisponível = 'I',
        Vendido = 'V',
        Permutado = 'P',
        Reservado = 'R',
        Garantia = 'G',
        Alugado = 'A'
    }
    public enum TipoQuitacao
    {
        DacaoEmPagamento = 'D',
        DireitoSobreAVenda = 'V'
    }
    public enum SimNao
    {
        Sim = 'S',
        Nao = 'N'
    }
    public enum TipoReceitaSPED
    {
        VO = 'O',
        VOmaisVM = 'V',
        VOmaisVMmaisJurosmaisVMJuros = 'J'
    }
    
    public class BemTerceiro
    {
        public int Id { get; set; }
        public int CodigoIndice { get; set; }
        public string Nome { get; set; }
        public TipoStatus Status { get; set; }
        public DateTime DataStatus { get; set; }
        public TipoQuitacao Quitacao { get; set; }
        public SimNao Alienado { get; set; }
        public decimal ValorVenda { get; set; }
        public decimal ValorPretendido { get; set; }
        public decimal ValorAutorizado { get; set; }
        public byte[] Observacao { get; set; }
        public decimal Utilizacao { get; set; }
        public DateTime DataCadastro { get; set; }
        public decimal ValorUtilizado { get; set; }
        public int CodigoProposta { get; set; }
        public string CodigoUnidadeImobiliaria { get; set; }
        public int CodigoNaturezaEspecifica { get; set; }
        public TipoReceitaSPED BaseReceitaSPED { get; set; }
        public SimNao ParticipaSPEDPISCOFINS { get; set; }
        public SimNao ReceitasNaoContratuais { get; set; }
        public SimNao GeraF500F525 { get; set; }
        public SimNao ContratosSemRecebimento { get; set; }
        public string SituacaoTributariaPIS { get; set; }
        public string SituacaoTributariaCOFINS { get; set; }
        public SimNao DescontoPISCOFINS { get; set; }
        public SimNao Encargos { get; set; }
        public CodigoComposto CodigoOrganizacao { get; set; }
        public CodigoComposto CentroCusto { get; set; }
        public CodigoComposto Projeto { get; set; }
        public TipoBem TipoBem { get; set; }
        public Contabilidade Contabilidade { get; set; }
        public IEnumerable<Avaliacao> Avaliacoes { get; set; }
        public IEnumerable<Despesa> Despesas { get; set; }

    }
}
