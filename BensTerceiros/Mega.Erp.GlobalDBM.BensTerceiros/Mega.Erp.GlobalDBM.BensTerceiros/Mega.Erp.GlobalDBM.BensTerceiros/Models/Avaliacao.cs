﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Models
{
    public class Avaliacao
    {
        public int Id { get; set; }
        public DateTime DataAvaliacao { get; set; }
        public String NomeAvaliador { get; set; }
        public decimal ValorMaximo { get; set; }
        public decimal ValorMinimo { get; set; }
    }
}
