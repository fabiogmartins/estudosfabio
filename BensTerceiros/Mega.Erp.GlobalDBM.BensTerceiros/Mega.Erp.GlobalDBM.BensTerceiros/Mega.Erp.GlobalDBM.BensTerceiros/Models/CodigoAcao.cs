﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Models
{
    public class CodigoAcao
    {
        public decimal Id { get; set; }
        public decimal Tabela { get; set; }
        public decimal Padrao { get; set; }
    }
}
