﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Models
{
    public class Despesa
    {
        public int Id { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataCadastro { get; set; }
        public String Historico { get; set; }
        public SimNao Agrega { get; set; }
    }
}
