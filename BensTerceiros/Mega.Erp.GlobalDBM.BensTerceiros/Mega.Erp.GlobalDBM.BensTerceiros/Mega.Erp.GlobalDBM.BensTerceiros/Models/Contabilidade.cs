﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Models
{
    public class Contabilidade
    {
        public int Id { get; set; }
        //Plano de Contas
        public CodigoComposto PlanoContasCurtoPrazo { get; set; }
        public CodigoComposto PlanoTransitoria { get; set; }
        public CodigoComposto PlanoJurosAtivos { get; set; }
        public CodigoComposto PlanoDistratoResultado { get; set; }
        public CodigoComposto PlanoDistratoPassivo { get; set; }
        public CodigoComposto PlanoREFVO { get; set; }
        public CodigoComposto PlanoREFVM { get; set; }
        public CodigoComposto PlanoPermuta { get; set; }
        public CodigoComposto PlanoCartaCredito { get; set; }
        public CodigoComposto PlanoDacaoPagamento { get; set; }
        public CodigoComposto PlanoDireitoSobreVenda { get; set; }

        //Ações
        public CodigoAcao AcaoInclusaoContrato { get; set; }
        public CodigoAcao AcaoTermoContratual { get; set; }
        public CodigoAcao AcaoMovimentoBaixa { get; set; }
        public CodigoAcao AcaoDistratoAPagar { get; set; }
        public CodigoAcao AcaoDistratoSaldo { get; set; }
        public CodigoAcao AcaoApropriacaoJuros { get; set; }
        public CodigoAcao AcaoRenegociacaoDebito { get; set; }
        public CodigoAcao AcaoRenegociacaoCredito { get; set; }
        public CodigoAcao AcaoContratoAluguel { get; set; }
        public CodigoAcao AcaoPermuta { get; set; }
        public CodigoAcao AcaoCartaCredito { get; set; }
        public CodigoAcao AcaoDacaoDePagamento { get; set; }
        public CodigoAcao AcaoDireitoSobreVenda { get; set; }
        public CodigoAcao AcaoTaxasAdicionais { get; set; }
        public CodigoAcao AcaoNaoContabiliza { get; set; }
        public CodigoAcao AcaoDistratoAPagarNC { get; set; }
        public CodigoAcao AcaoDistratoTermoAPagar { get; set; }
        public CodigoAcao AcaoDistratoTermoSaldo { get; set; }

    }

}

