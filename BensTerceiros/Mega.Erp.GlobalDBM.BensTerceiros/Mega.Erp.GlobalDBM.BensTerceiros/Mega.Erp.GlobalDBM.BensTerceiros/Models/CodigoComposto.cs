﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Models
{
    public class CodigoComposto
    {
        public int Id { get; set; }
        public int Tabela { get; set; }
        public int Padrao { get; set; }
        public string TipoIndetificador { get; set; }
    }
}
