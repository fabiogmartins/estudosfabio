﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.GlobalDBM.BensTerceiros.Models
{
    public enum TipoCategoria
    {
        Veiculos = 'V',
        Imoveis = 'I',
        Outros = 'O'
    }

    public enum TipoCombustivel
    {
        Gasolina = 'G',
        Alcool = 'A',
        Diesel = 'D',
        GasNatural = 'S',
        Eletrico = 'E',
        Outros = 'O'
    }

    public class TipoBem
    {
        public int CodigoTipoBem { get; set; }
        public TipoCategoria Categoria { get; set; }
        //Se for do tipo Imoveis
        public decimal AreaTotal { get; set; }
        public decimal AreaConstruida { get; set; }
        public int Dependencias { get; set; }
        //Se for do tipo Veiculos
        public string Placa { get; set; }
        public decimal Kilometragem { get; set; }
        public TipoCombustivel Combustivel { get; set; }

    }
}
